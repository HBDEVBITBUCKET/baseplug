/********* BasePlug.h Cordova Plugin Implementation *******/

#import <Cordova/CDVPlugin.h>
#import "BasePlug.h"

@interface BasePlug : CDVPlugin
{}

@property (nonatomic, copy) NSString* callbackId;

- (void) baseMethod:(CDVInvokedUrlCommand*)command;

@end
