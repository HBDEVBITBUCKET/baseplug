function BasePlug() {
}

BasePlug.prototype.baseMethod = function (options, successCallback, errorCallback) {
  cordova.exec(successCallback, errorCallback, "BasePlug", "baseMethod", [options]);
};


BasePlug.install = function () {
  if (!window.plugins) {
    window.plugins = {};
  }

  window.plugins.BasePlug = new BasePlug();
  return window.plugins.BasePlug;
};

cordova.addConstructor(BasePlug.install);